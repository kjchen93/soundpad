# Soundpad

A 4-by-4 launchpad application created in Qt. Each row represents a category of sound and each column is a different version of the sound. From top to bottom the sound of the rows are respectively: Kick drum, hihat, snare drum and vocal. Pressing a button will cause the button to flash a vibrant green color and the sound will be played accordingly. An image of the application is shown in the picture below.

![](/soundpad.jpg)

## Requirements
`QtMultimedia`, `Qt 6.0.0`
## Author

[Kai Chen](gitlab.com/kjchen93)