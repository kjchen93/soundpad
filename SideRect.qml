import QtQuick 2.0


Rectangle {
    id: myRect

    property FocusRect focusItem

    width: 50; height: 50
    radius: 6
    color: "#646464"
    border.width: 4; border.color: "white"

    MouseArea {
        anchors.fill: parent
        //hoverEnabled: true
        onClicked: {
            myRect.focusItem.x = myRect.x;
            myRect.focusItem.y = myRect.y;
        }
    }
}
