import QtQuick 2.0
import QtQuick.Window 2.15
import QtMultimedia
import QtQuick.Layouts


Window {
    width: 500; height: 500
    color: "#343434"
    visible: true;

    SoundEffect {
        id: kick1
        source:"qrc:/audio/kick1.wav"
    }
    SoundEffect {
        id: kick2
        source:"qrc:/audio/kick2.wav"
    }
    SoundEffect {
        id: kick3
        source:"qrc:/audio/kick3.wav"
    }
    SoundEffect {
        id: kick4
        source:"qrc:/audio/kick4.wav"
    }
    SoundEffect {
        id: hhat1
        source:"qrc:/audio/hihat1.wav"
    }
    SoundEffect {
        id: hhat2
        source:"qrc:/audio/hihat2.wav"
    }
    SoundEffect {
        id: hhat3
        source:"qrc:/audio/hihat3.wav"
    }
    SoundEffect {
        id: hhat4
        source:"qrc:/audio/hihat4.wav"
    }
    SoundEffect {
        id: snare1
        source:"qrc:/audio/snare1.wav"
    }
    SoundEffect {
        id: snare2
        source:"qrc:/audio/snare2.wav"
    }
    SoundEffect {
        id: snare3
        source:"qrc:/audio/snare3.wav"
    }
    SoundEffect {
        id: snare4
        source:"qrc:/audio/snare4.wav"
    }
    SoundEffect {
        id: vocal1
        source:"qrc:/audio/vocal1.wav"
    }
    SoundEffect {
        id: vocal2
        source:"qrc:/audio/vocal2.wav"
    }
    SoundEffect {
        id: vocal3
        source:"qrc:/audio/vocal3.wav"
    }
    SoundEffect {
        id: vocal4
        source:"qrc:/audio/vocal4.wav"
    }




    ColumnLayout{

        spacing:  20
        RowLayout{
            Layout.topMargin: 20
            Layout.leftMargin: 20
            spacing: 20
            Rectangle {
                width: 100
                height: 100
                color: "#3498DB"
                opacity: 1
                radius: 8

                MouseArea {
                    anchors.fill: parent

                    onClicked: {
                        kick1.play()
                        animation41.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation41
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#3498DB"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#3498DB"
                opacity: 0.8
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        kick2.play()
                        animation42.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation42
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#3498DB"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#3498DB"
                opacity: 0.6
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        kick3.play()
                        animation43.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation43
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#3498DB"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#3498DB"
                opacity: 0.4
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        kick4.play()
                        animation44.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation44
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#3498DB"; duration: 100 }
                }
            }
        }
        RowLayout{
            Layout.leftMargin: 20
            spacing: 20
            Rectangle {
                width: 100
                height: 100
                color: "#2E86C1"
                opacity: 1
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        hhat1.play()
                        animation31.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation31
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#2E86C1"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#2E86C1"
                opacity: 0.8
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        hhat2.play()
                        animation32.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation32
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#2E86C1"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#2E86C1"
                opacity: 0.6
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        hhat3.play()
                        animation33.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation33
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#2E86C1"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#2E86C1"
                opacity: 0.4
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        hhat4.play()
                        animation34.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation34
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#2E86C1"; duration: 100 }
                }
            }
        }
        RowLayout{
            Layout.leftMargin: 20
            spacing: 20
            Rectangle {
                width: 100
                height: 100
                color: "#2874A6"
                opacity: 1
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        snare1.play()
                        animation21.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation21
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#2874A6"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#2874A6"
                opacity: 0.8
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        snare2.play()
                        animation22.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation22
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#2874A6"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#2874A6"
                opacity: 0.6
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        snare3.play()
                        animation23.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation23
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#2874A6"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#2874A6"
                opacity: 0.4
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        snare4.play()
                        animation24.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation24
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#2874A6"; duration: 100 }
                }
            }
        }
        RowLayout{
            Layout.leftMargin: 20
            spacing: 20
            Rectangle {
                width: 100
                height: 100
                color: "#21618C"
                opacity: 1
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        vocal1.play()
                        animation1.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation1
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#21618C"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#21618C"
                opacity: 0.8
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        vocal2.play()
                        animation2.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation2
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#21618C"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#21618C"
                opacity: 0.6
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        vocal3.play()
                        animation3.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation3
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#21618C"; duration: 100 }
                }
            }
            Rectangle {
                width: 100
                height: 100
                color: "#21618C"
                opacity: 0.4
                radius: 8
                MouseArea {
                    anchors.fill: parent

                    onClicked : {
                        vocal4.play()
                        animation4.start();
                    }
                }
                SequentialAnimation on color {
                    id: animation4
                    running: false

                    ColorAnimation { to: "#2ECC71"; duration: 50 }
                    ColorAnimation { to: "#21618C"; duration: 100 }
                }
            }
        }

    }

}





